#include "GuiMain.h"

// This is included here because it is forward declared in
#include "ui_GuiMain.h"

#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QItemSelectionModel>

#include "AnnotatedImage.h"
#include "Converter.h"

// Constructor
GuiMain::GuiMain()
{
	this->anI = new AnnotatedImage();
	this->ui = new Ui_GuiMain;
	this->ui->setupUi(this);
	
	this->ui->evaluationTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	this->ui->globalTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	this->ui->evaluationTable->setHorizontalHeaderLabels({"segment", "precision", "recall", "accuracy", "F1-score"});


	// Set up action signals and slots
	connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));
	connect(this->ui->actionLoad_folder, SIGNAL(triggered()), this, SLOT(loadFolder()));
	connect(this->ui->actionNext, SIGNAL(triggered()), this, SLOT(openNext()));
	connect(this->ui->actionBack, SIGNAL(triggered()), this, SLOT(openBack()));
	connect(this->ui->segList, SIGNAL(itemSelectionChanged()), this, SLOT(showSegImage()));
	connect(this->ui->annList, SIGNAL(itemSelectionChanged()), this, SLOT(showAnnImage()));

	connect(this->ui->okButton, SIGNAL(clicked()), this, SLOT(addSegment()));
	connect(this->ui->delButton, SIGNAL(clicked()), this, SLOT(deleteSegment()));
	connect(this->ui->saveButton, SIGNAL(clicked()), this, SLOT(saveGlobals()));
}

void GuiMain::slotExit() {
	qApp->exit();
}

void GuiMain::loadFolder()
{
	baseFolderPath = QFileDialog::getExistingDirectory(nullptr, tr("Select parent folder"), QString(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
	if (baseFolderPath.isEmpty())
	{
		return;
	}
	QDir mainDir = QDir(baseFolderPath);
	folders = mainDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::SortFlag::Name);
	iFolder = 0;
	loadGlobals();
	openNext();
}

void GuiMain::openNext()
{
	if (iFolder + 1 < folders.size()) 
	{
		open(++iFolder);
	} else
	{
		iFolder = 0;
		open(iFolder);
	}
}

void GuiMain::openBack()
{
	if (iFolder - 1 < folders.size())
	{
		open(--iFolder);
	} 
	else
	{
		iFolder = folders.size() - 1;
		open(iFolder);
	}
}

void GuiMain::showSegImage()
{
	if (ui->segList->selectedItems().count() > 0) {
		auto segItem = ui->segList->selectedItems()[0];
		auto segPath = segItem->text();
		QPixmap segmentImage(segPath);
		ui->segImage->setPixmap(segmentImage);
	}
}

void GuiMain::showAnnImage()
{
	cv::Mat image;
	QListWidget* annsList = ui->annList;
	if (annsList->selectedItems().size() > 0)
	{
		std::vector<int> annotations;
		for (auto &annItem : annsList->selectedItems()) {
			auto ann = annItem->text();
			ann = ann.split(" ")[0];
			annotations.push_back(ann.toInt());
		}
		image = anI->show(annotations);
	} 
	else
	{
		image = anI->show();
	}
	QImage qImage = MatToQImage(image);
	ui->annImage->setPixmap(QPixmap::fromImage(qImage));
}

void GuiMain::addSegment()
{
	QListWidget* annsList = ui->annList;
	QListWidget* segsList = ui->segList;
	QTableWidget* evaluationTable = ui->evaluationTable;
	QTableWidget* globalTable = ui->globalTable;

	if (annsList->selectedItems().count() > 0 
		&& segsList->selectedItems().count() > 0 
		&& globalTable->selectedItems().count() > 0)
	{
		std::vector<int> annotations;
		for (auto &annItem : annsList->selectedItems()) {
			auto ann = annItem->text();
			ann = ann.split(" ")[0];
			annotations.push_back(ann.toInt());
		}
		cv::Mat objectMask = anI->showMask(annotations);
		auto segItem = segsList->selectedItems()[0];
		auto segmentPath = segItem->text().toStdString();
		auto metrics = anI->compare(objectMask, segmentPath);
		
		int row = ui->evaluationTable->rowCount();
		evaluationTable->insertRow(row);
		QTableWidgetItem *qtwi0 = new QTableWidgetItem(segItem->text());
		QTableWidgetItem *qtwi1 = new QTableWidgetItem(QString::number(metrics[0]));
		QTableWidgetItem *qtwi2 = new QTableWidgetItem(QString::number(metrics[1]));
		QTableWidgetItem *qtwi3 = new QTableWidgetItem(QString::number(metrics[2]));
		QTableWidgetItem *qtwi4 = new QTableWidgetItem(QString::number(metrics[3]));
		evaluationTable->setItem(row, 0, qtwi0);
		evaluationTable->setItem(row, 1, qtwi1);
		evaluationTable->setItem(row, 2, qtwi2);
		evaluationTable->setItem(row, 3, qtwi3);
		evaluationTable->setItem(row, 4, qtwi4);
		
		auto select = globalTable->selectionModel();
		auto selectedRowGlobal = select->selectedRows()[0];

		auto precision = globalTable->item(selectedRowGlobal.row(), 0);
		auto recall= globalTable->item(selectedRowGlobal.row(), 1);
		auto accuracy = globalTable->item(selectedRowGlobal.row(), 2);
		auto f1 = globalTable->item(selectedRowGlobal.row(), 3);

		double new_prec = (precision->text().toDouble()*evaluatedSegmentsCount[selectedRowGlobal.row()] + metrics[0]);
		double new_rec = (recall->text().toDouble()*evaluatedSegmentsCount[selectedRowGlobal.row()] + metrics[1]);
		double new_acc = (accuracy->text().toDouble()*evaluatedSegmentsCount[selectedRowGlobal.row()] + metrics[2]);
		double new_f1 = (f1->text().toDouble()*evaluatedSegmentsCount[selectedRowGlobal.row()] + metrics[3]);
		evaluatedSegmentsCount[selectedRowGlobal.row()]++;
		new_prec /= static_cast<double>(evaluatedSegmentsCount[selectedRowGlobal.row()]);
		new_rec  /= static_cast<double>(evaluatedSegmentsCount[selectedRowGlobal.row()]);
		new_acc  /= static_cast<double>(evaluatedSegmentsCount[selectedRowGlobal.row()]);
		new_f1   /= static_cast<double>(evaluatedSegmentsCount[selectedRowGlobal.row()]);
		precision->setText(QString::number(new_prec));
		recall->setText(QString::number(new_rec));
		accuracy->setText(QString::number(new_acc));
		f1->setText(QString::number(new_f1));
		segItem->setBackground(Qt::green);
	}
}

void GuiMain::deleteSegment()
{
	QTableWidget* pairsTable = ui->evaluationTable;

	pairsTable->removeRow(pairsTable->currentRow());
}

void GuiMain::open(int i)
{
	basePath = baseFolderPath + "/" + folders[iFolder];
	this->setWindowTitle("Object 2 Segment - " + basePath);
	auto image = basePath + "/image/";
	depthPath = basePath + "/depth/";
	anI->setFile((basePath + "/annotation/index.json").toStdString());
	auto segments = basePath + "/image/segments/";

	try {
		// look for an image *.jpg
		QDir imageF = QDir(image);
		QStringList filters;
		filters << "*.jpg";
		imageF.setNameFilters(filters);
		imageF.setFilter(QDir::Files | QDir::NoDotAndDotDot);

		imagePath = image + imageF.entryList()[0];
		anI->setImage(imagePath.toStdString());

		// look for an depth *.png
		QDir imageP = QDir(depthPath);
		filters.empty();
		filters << "*.png";
		imageP.setNameFilters(filters);
		imageP.setFilter(QDir::Files | QDir::NoDotAndDotDot);

		depthPath = depthPath + imageP.entryList()[0];

		// look for segments
		filters.empty();
		filters << "*.png";
		QDir segmentsF = QDir(segments);

		segmentsF.setNameFilters(filters);
		segmentsF.setFilter(QDir::Files | QDir::NoDotAndDotDot);
		auto listOfSegments = segmentsF.entryList();
		semgentsPaths.clear();
		for (auto segment : listOfSegments) {
			semgentsPaths.push_back(segments + segment);
		}

		showSegsInList();
		showAnnsInList();
		showAnnImage();
		cv::Mat depthMat = cv::imread(depthPath.toStdString(), cv::IMREAD_UNCHANGED);
		cv::imshow("depth", depthMat);
	} catch (std::exception e)
	{
		std::string warningMsg = "Exception: " + std::string(e.what());
		std::cout << warningMsg << std::endl;
		QMessageBox::warning(this, tr("Wrong input data!"),
			QString::fromStdString(warningMsg), QMessageBox::Ok,
			QMessageBox::Ok);
	}
}

void GuiMain::showSegsInList()
{
	auto segList = ui->segList;
	segList->clear();
	for (auto segment : semgentsPaths)
	{
		segList->addItem(segment);
	}
}

void GuiMain::showAnnsInList()
{
	auto annsList = ui->annList;
	annsList->clear();
	auto annotations = anI->getObjectsList();
	for (auto ann : annotations)
	{
		annsList->addItem(QString::fromStdString(ann));
	}
}

void GuiMain::saveGlobals() {
	QTableWidget* globals = ui->globalTable;

	QFile evaluationFile(baseFolderPath + "/values.txt");

	if (evaluationFile.open(QIODevice::Truncate | QIODevice::ReadWrite)) {
		QTextStream stream(&evaluationFile);
		for (auto i = 0; i < evaluatedSegmentsCount.size(); i++) {
			stream << QString::number(evaluatedSegmentsCount[i]) << ";";
		}
		stream << "\n";
		for (auto r = 0; r < globals->rowCount(); r++) {
			for (auto c = 0; c < globals->columnCount(); c++) {
				stream << globals->item(r, c)->text().toDouble() << ";";
			}
			stream << "\n";
		}
		evaluationFile.close();
	}
}

void GuiMain::loadGlobals()
{
	QTableWidget* globals = ui->globalTable;
	
	QFile evaluationFile(baseFolderPath + "/values.txt");
	try 
	{
		if (evaluationFile.open(QIODevice::ReadOnly)) {
			QTextStream stream(&evaluationFile);
			QString firstLine = stream.readLine();
			QStringList counts = firstLine.split(";", QString::SkipEmptyParts);
			evaluatedSegmentsCount.clear();
			for (auto c : counts)
			{
				evaluatedSegmentsCount.push_back(c.toInt());
			}
			for (auto r = 0; r < globals->rowCount(); r++) {
				QString line = stream.readLine();
				QStringList values = line.split(";");

				for (auto c = 0; c < globals->columnCount(); c++) {
					globals->item(r, c)->setText(values[c]);
				}
				stream << "\n";
			}
			evaluationFile.close();	
		}
	}
	catch (std::exception e)
	{
		std::cout << "Problem loading file " + evaluationFile.fileName().toStdString() << std::endl;
	}
}
