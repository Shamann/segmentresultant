#include <QApplication>

#include "GuiMain.h"


int main(int argc, char* argv[]) {

	QApplication app(argc, argv);
	GuiMain guiMain;
	guiMain.show();

	return app.exec();
}
