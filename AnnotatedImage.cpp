#include "AnnotatedImage.h"
#include "rapidjson/document.h"
#include "rapidjson/istreamwrapper.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <exception>

using namespace rapidjson;

void AnnotatedImage::loadObjects()
{
	m_names.clear();
	m_objects.clear();
	std::ifstream jsonFile(m_annotationFile);
	IStreamWrapper isw(jsonFile);

	Document d;
	d.ParseStream(isw);

	if (!d.IsObject()) return;

	if (!d.HasMember("frames")) return;
	auto frames = d["frames"].GetArray();
	auto objects = d["objects"].GetArray();

	auto polygon = frames[0].GetObject();
	const Value &polygons = polygon["polygon"];
	for (auto& p : polygons.GetArray()) {
		if (p.HasMember("x") && p.HasMember("y")) {
			if (p["x"].IsArray() && p["y"].IsArray()) {
				auto x = p["x"].GetArray();
				auto y = p["y"].GetArray();

				int object = p["object"].GetInt();

				std::vector<cv::Point> an;
				for (auto i = 0; i < x.Size(); i++) {
					int xx = x[i].GetFloat();
					int yy = y[i].GetFloat();;
					//xx = xx < 0 ? 0 : xx > m_image.cols ? m_image.cols : xx;
					//yy = yy < 0 ? 0 : yy > m_image.rows ? m_image.rows : yy;
					an.push_back(cv::Point(xx, yy));
				}
				m_objects.push_back(an);
				if (object < objects.Size()) {
					m_names.push_back(objects[object]["name"].GetString());
				}
				else {
					m_names.push_back("NaN");
				}
			}
		}
	}
}

AnnotatedImage::AnnotatedImage()
{
}

AnnotatedImage::AnnotatedImage(std::string annFile) : m_annotationFile(annFile)
{
	loadObjects();
}

AnnotatedImage::~AnnotatedImage()
{
}

cv::Point AnnotatedImage::centroid(std::vector<cv::Point> polygon)
{
	cv::Point centroid{ 0, 0 };
	
	for (auto i = 0; i < polygon.size(); ++i)
	{
		centroid += polygon[i];
	}
	centroid /= int(polygon.size());
	
	return centroid;
}

void AnnotatedImage::setFile(std::string annFile)
{
	m_annotationFile = annFile;
	loadObjects();
}

void AnnotatedImage::setImage(std::string imageFile)
{
	m_image = cv::imread(imageFile, cv::IMREAD_UNCHANGED);
}

void AnnotatedImage::setImage(cv::Mat image)
{
	m_image = image;
}

std::vector<std::string> AnnotatedImage::getObjectsList()
{
	std::vector<std::string> segments;
	for (auto i = 0; i < m_objects.size(); i++)
	{
		segments.push_back(std::to_string(i) + " - " + m_names[i]);
	}
	return segments;
}

cv::Mat AnnotatedImage::show(int i)
{
	cv::Mat tmp = m_image.clone();
	long color = 0x0000ff;
	if (i == -1) {
		for (auto idx = 0; idx < m_objects.size(); idx++) {
			color += idx * 0xff00 / m_objects.size();
			cv::Scalar rgb = cv::Scalar(color >> 16, (color & 0xff00) >> 8, color & 0xff);
			cv::drawContours(tmp, m_objects, idx, rgb, 2);
			cv::putText(tmp, m_names[idx], centroid(m_objects[idx]), CV_FONT_HERSHEY_PLAIN, 1.7, rgb, 2);
		}
	}
	else
	{
		color += i * 0xff00 / m_objects.size();
		cv::Scalar rgb = cv::Scalar(color >> 16, (color & 0xff00) >> 8, color & 0xff);
		cv::drawContours(tmp, m_objects, i, rgb, 2);
		cv::putText(tmp, m_names[i], centroid(m_objects[i]), CV_FONT_HERSHEY_PLAIN, 1.7, rgb, 2);
	}
	cv::cvtColor(tmp, tmp, CV_BGR2RGB);
	return tmp;
}

cv::Mat AnnotatedImage::show(std::vector<int> is)
{
	cv::Mat tmp = m_image.clone();
	long color = 0x0000ff;
	if (is.empty()) {
		for (auto idx = 0; idx < m_objects.size(); idx++) {
			color += idx * 0xff00 / m_objects.size();
			cv::Scalar rgb = cv::Scalar(color >> 16, (color & 0xff00) >> 8, color & 0xff);
			cv::drawContours(tmp, m_objects, idx, rgb, 2);
			cv::putText(tmp, m_names[idx], centroid(m_objects[idx]), CV_FONT_HERSHEY_PLAIN, 1.7, rgb, 2);
		}
	}
	else {
		for (auto idx : is) {
			color += idx * 0xff00 / m_objects.size();
			cv::Scalar rgb = cv::Scalar(color >> 16, (color & 0xff00) >> 8, color & 0xff);
			cv::drawContours(tmp, m_objects, idx, rgb, 2);
			cv::putText(tmp, m_names[idx], centroid(m_objects[idx]), CV_FONT_HERSHEY_PLAIN, 1.7, rgb, 2);
		}
	}
	cv::cvtColor(tmp, tmp, CV_BGR2RGB);
	return tmp;
}

cv::Mat AnnotatedImage::showMask(std::vector<int> is)
{
	cv::Mat tmp = cv::Mat::zeros(m_image.rows, m_image.cols, CV_8UC1);

	for (auto idx : is) {
		cv::drawContours(tmp, m_objects, idx, cv::Scalar(255, 255, 255), CV_FILLED);
	}
	
	return tmp;
}

std::vector<double> AnnotatedImage::compare(const cv::Mat& objectMask, const std::string segmentPath)
{
	cv::Mat segment = cv::imread(segmentPath, CV_LOAD_IMAGE_UNCHANGED);
	cv::cvtColor(segment, segment, CV_BGR2GRAY);
	cv::Mat positive, negative, objectMaskNegative;
	cv::threshold(segment, positive, 0, 255, CV_THRESH_BINARY);
	cv::threshold(segment, negative, 0, 255, CV_THRESH_BINARY_INV);
	cv::threshold(objectMask, objectMaskNegative, 0, 255, CV_THRESH_BINARY_INV);

	//cv::imshow("object", objectMask);
	//cv::imshow("object_negative", objectMaskNegative);
	//cv::imshow("segment", positive);
	//cv::imshow("segment_negative", negative);
	cv::Mat tpM, tnM, fpM, fnM;
	cv::bitwise_and(objectMask, positive, tpM);
	cv::bitwise_and(objectMaskNegative, negative, tnM);
	cv::bitwise_and(objectMaskNegative, positive, fpM);
	cv::bitwise_and(objectMask, negative, fnM);
	//cv::imshow("tp", tpM);
	//cv::imshow("tn", tnM);
	//cv::imshow("fp", fpM);
	//cv::imshow("fn", fnM);

	double tp, tn, fp, fn;
	tp = cv::countNonZero(tpM);
	tn = cv::countNonZero(tnM);
	fp = cv::countNonZero(fpM);
	fn = cv::countNonZero(fnM);

	std::vector<double> metrics;
	metrics.push_back(precision(tp, tn, fp, fn));
	metrics.push_back(recall(tp, tn, fp, fn));
	metrics.push_back(accuracy(tp, tn, fp, fn));
	metrics.push_back(f1_score(tp, tn, fp, fn));

	return metrics;
}

double AnnotatedImage::precision(double tp, double tn, double fp, double fn)
{
	if (tp + fp > 0)
	{
		return tp / (tp + fp);
	}
	return 0;
}

double AnnotatedImage::recall(double tp, double tn, double fp, double fn)
{
	if (tp + fn > 0)
	{
		return tp / (tp + fn);
	}
	return 0;
}

double AnnotatedImage::accuracy(double tp, double tn, double fp, double fn) {
	if (tp + tn + fp + fn > 0) {
		return (tp + tn) / (tp + fp + fn + tn);
	}
	return 0;
}

double AnnotatedImage::f1_score(double tp, double tn, double fp, double fn) {
	if (tp + fp + fn > 0) {
		return 2 * tp / (2 * tp + fp + fn);
	}
	return 0;
}
