#pragma once
#include "opencv2/opencv.hpp"


class AnnotatedImage
{
	std::string m_annotationFile;
	cv::Mat m_image;
	std::vector<std::vector<cv::Point>> m_objects;
	std::vector<std::string> m_names;

	void loadObjects();

public:
	AnnotatedImage();
	AnnotatedImage(std::string annFile);
	~AnnotatedImage();
	static cv::Point centroid(std::vector<cv::Point> polygon);
	void setFile(std::string annFile);
	void setImage(std::string imageFile);
	void setImage(cv::Mat image);
	std::vector<std::string> getObjectsList();
	cv::Mat show(int i = -1);
	cv::Mat show(std::vector<int> is);
	cv::Mat showMask(std::vector<int> is);
	// precision, recall, accuracy, f1_score
	std::vector<double> compare(const cv::Mat &objectMask, const std::string segmentPath);

	static double precision(double tp, double tn, double fp, double fn);
	static double recall(double tp, double tn, double fp, double fn);
	static double accuracy(double tp, double tn, double fp, double fn);
	static double f1_score(double tp, double tn, double fp, double fn);
};

