#include <QMainWindow>



// Forward Qt class declarations
class Ui_GuiMain;
class AnnotatedImage;

class GuiMain : public QMainWindow {
	Q_OBJECT
public:

	// Constructor/Destructor
	GuiMain();
	~GuiMain() {};

	public slots:

	virtual void slotExit();
	virtual void loadFolder();
	virtual void openNext();
	virtual void openBack();
	virtual void showSegImage();
	virtual void showAnnImage();

	virtual void addSegment();
	virtual void deleteSegment();
	virtual void saveGlobals();
	virtual void loadGlobals();

private:
	// UI methods
	void open(int i);
	void showSegsInList();
	void showAnnsInList();

	// Designer form
	Ui_GuiMain *ui;

	// data
	QString baseFolderPath;
	QStringList folders;
	size_t iFolder;
	QString basePath;
	QString imagePath;
	QString depthPath;
	std::vector<QString> semgentsPaths;

	std::vector<int> evaluatedSegmentsCount = { 0,0,0,0,0,0 };

	AnnotatedImage *anI;
	
};